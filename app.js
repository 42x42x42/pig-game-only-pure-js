/*
Pig game

GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLOBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game (win score could be changed in input field)

++++++++
- if player rolls 6 two times in a row he also lose his round

*/
'use strict';
var scores, roundScore, activePlayer, gameIsPlaying, previousDice,maxScore;

initGame();
//ROLL
document.querySelector('.btn-roll').addEventListener('click', function () {
    if (gameIsPlaying) {
        // 1. random number
        var dice = Math.floor(Math.random() * 6) + 1;
        // var dice = Math.round(Math.random()) + 5; //for test of 2 times 6
        // 2. display result
        var diceDOM = document.querySelector('.dice');
        diceDOM.style.display = 'block';
        diceDOM.src = 'dice-' + dice + '.png';

        // 3. Update the round score IF the rolled number was not null
        if (dice !== 1 && !(previousDice===6&&dice===6) ) {
            console.log('prev dice: '+previousDice);
            console.log('dice: '+dice);
            console.log(previousDice===dice && dice===6);

            console.log('-------');

            roundScore += dice;
            previousDice=dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        }
        else {
            nextPlayer();
        }
    }
});

//HOLD
document.querySelector('.btn-hold').addEventListener('click', function () {
    if (gameIsPlaying) {
        //add Current score to global
        scores[activePlayer] += roundScore;
        //update UI
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
        //check if win
        if (scores[activePlayer] >= maxScore) {
            document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.querySelector('.dice').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gameIsPlaying = false;
        }
        else {
            //next player
            nextPlayer();
        }
    }

});

document.querySelector('.btn-new').addEventListener('click', initGame);
document.querySelector('.input-score-inp').addEventListener('input',function () {
    maxScore=+(document.querySelector('.input-score-inp').value);
    console.log('input'+maxScore);
});

function initGame() {
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    gameIsPlaying = true;
    previousDice=0;
    maxScore=+(document.querySelector('.input-score-inp').value);


    document.querySelector('.dice').style.display = 'none';
    document.getElementById('score-0').textContent = 0;
    document.getElementById('score-1').textContent = 0;
    document.getElementById('current-0').textContent = 0;
    document.getElementById('current-1').textContent = 0;
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    //remove and add to be sure that there will be no 2 active class if its winner
    document.querySelector('.player-0-panel').classList.add('active');


    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');

}

function nextPlayer() {
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundScore = 0;
    previousDice=0;

    document.getElementById('current-0').textContent = 0;
    document.getElementById('current-1').textContent = 0;

    //toogle add class if its not there or delete it if its there
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
    //hide dice if 1
    document.querySelector('.dice').style.display = 'none';

}